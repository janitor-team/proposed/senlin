#!/usr/bin/make -f

include /usr/share/openstack-pkg-tools/pkgos.make

LAST_CHANGE = $(shell dpkg-parsechangelog -S Date)
BUILD_DATE  = $(shell LC_ALL=C date -u "+%B %d, %Y" -d "$(LAST_CHANGE)")
SPHINXOPTS := -D html_last_updated_fmt="$(BUILD_DATE)"

%:
	dh $@ --buildsystem=python_distutils --with python3,sphinxdoc,systemd

override_dh_auto_clean:
	python3 setup.py clean

override_dh_clean:
	rm -rf build
	rm -f debian/*.init debian/*.service debian/*.upstart
	rm -f debian/senlin-common.config debian/senlin-common.postinst debian/senlin-api.config debian/senlin-api.postinst
	rm -rf debian/CHANGEME-common.postrm debian/*.templates debian/po
	dh_clean

override_dh_auto_build:
	/usr/share/openstack-pkg-tools/pkgos_insert_include pkgos_func senlin-common.config
	/usr/share/openstack-pkg-tools/pkgos_insert_include pkgos_func senlin-common.postinst
	/usr/share/openstack-pkg-tools/pkgos_insert_include pkgos_func senlin-api.config
	/usr/share/openstack-pkg-tools/pkgos_insert_include pkgos_func senlin-api.postinst
	/usr/share/openstack-pkg-tools/pkgos_insert_include pkgos_postrm senlin-common.postrm
	pkgos-merge-templates senlin-api senlin endpoint
	pkgos-merge-templates senlin-common senlin db rabbit ksat

override_dh_sphinxdoc:
ifeq (,$(findstring nodocs, $(DEB_BUILD_OPTIONS)))
	PYTHONPATH=. PYTHON=python3 python3 -m sphinx $(SPHINXOPTS) -b html doc/source $(CURDIR)/debian/senlin-doc/usr/share/doc/senlin-doc/html
	rm $(CURDIR)/debian/senlin-doc/usr/share/doc/senlin-doc/html/search.html
	rm -rf $(CURDIR)/debian/senlin-doc/usr/share/doc/senlin-doc/html/.doctrees
	dh_sphinxdoc
endif

override_dh_auto_test:
	echo "Do nothing..."

override_dh_install:
	echo "Do nothing..."

override_dh_auto_install:
	rm -rf $(CURDIR)/debian/tmp
	for i in $(PYTHON3S) ; do \
		python$$i setup.py install --install-layout=deb --root $(CURDIR)/debian/tmp ; \
	done

ifeq (,$(findstring nocheck, $(DEB_BUILD_OPTIONS)))
	PYTHONPATH=$(CURDIR)/debian/tmp/usr/lib/python3/dist-packages pkgos-dh_auto_test --no-py2 'senlin\.tests\.unit\.(?!(.*engine\.test_health_manager\.TestNodePollUrlHealthCheck\.test_run_health_check_unhealthy.*|.*engine\.test_health_manager\.TestNodePollUrlHealthCheck\.test_run_health_check_conn_error.*))'
endif
	cp -auxf senlin/db/sqlalchemy/migrate_repo $(CURDIR)/debian/tmp/usr/lib/python3/dist-packages/senlin/db/sqlalchemy
	mkdir -p $(CURDIR)/debian/senlin-common/usr/share/senlin-common
	PYTHONPATH=$(CURDIR)/debian/tmp/usr/lib/python3/dist-packages oslo-config-generator \
		--output-file $(CURDIR)/debian/senlin-common/usr/share/senlin-common/senlin.conf \
		--wrap-width 140 \
		--namespace senlin.conf \
		--namespace keystonemiddleware.auth_token \
		--namespace oslo.db \
		--namespace oslo.log \
		--namespace oslo.messaging \
		--namespace oslo.middleware.cors \
		--namespace oslo.middleware.http_proxy_to_wsgi \
		--namespace oslo.policy \
		--namespace oslo.service.periodic_task \
		--namespace oslo.service.service \
		--namespace oslo.service.sslutils \
		--namespace osprofiler

	pkgos-readd-keystone-authtoken-missing-options $(CURDIR)/debian/senlin-common/usr/share/senlin-common/senlin.conf keystone_authtoken senlin

	mkdir -p $(CURDIR)/debian/senlin-common/etc/senlin/policy.d
	PYTHONPATH=$(CURDIR)/debian/tmp/usr/lib/python3/dist-packages oslopolicy-sample-generator \
		--output-file $(CURDIR)/debian/senlin-common/etc/senlin/policy.d/00_default_policy.yaml \
		--format yaml \
		--namespace senlin

	# Use the policy.d folder
	pkgos-fix-config-default $(CURDIR)/debian/senlin-common/usr/share/senlin-common/senlin.conf oslo_policy policy_dirs /etc/senlin/policy.d

	dh_install
	rm -rf $(CURDIR)/debian/tmp/usr/etc
	dh_missing --fail-missing

override_dh_python3:
	dh_python3 --shebang=/usr/bin/python3
